var path = require('path');
var fs = require('fs');
var UglifyJS = require("uglify-js");
var extend = require('util')._extend;
var log = require('winston');

exports.package = function(options, cb){

	log.level = options.log_level;

	prepareOptions(options, function(options){
		require('./package.js')(options, cb);
	})

}


function packageFilepath(options){
	return path.join(options.outputDir, packageFilename(options, options) );
}

function packageFilename(options){
	return [options.project, '-', options.version, '.js'].join('');
}


function readPackageJson(){
	var projectJsonPath = path.resolve('project.json');
	if(!fs.existsSync(projectJsonPath)) throw "Can't find project.json. Looked here: " + projectJsonPath;
	return require(projectJsonPath);
}


function gitVersion(options, cb){
	options.fn.gitVersion(options.packageJson.version, function(error, v){

		if(error) return  cb(error);

		options.version = v;

		log.info('Git Version Calculated: %s', options.version);

		cb(null, options);
	});
}


function prepareOptions(options, cb){

	options.packageJson = readPackageJson();
	options.outputDir = options.outputDir || ['..','dist', options.project].join(path.sep);


	gitVersion(options, function(err, options){
		options.packageFilepath =  packageFilepath(options);
		options.packageFilename = packageFilename(options);
		return cb(options);
	});


}
