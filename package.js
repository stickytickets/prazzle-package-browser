var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');
var UglifyJS = require("uglify-js");
var log = require('winston');

module.exports = function(options, cb)
{
	log.info('Packing browser App To: %s', options.outputDir);

	var packageFile = path.resolve(options.packageFilepath);

	//return if already exists.
	if(fs.existsSync(packageFile)){
		log.info('Package already exists: ', packageFile);
		return cb(null, createResult(options));
	}

	async.waterfall([
		async.apply(runNpmBuild, options),
		updateJsFilesToPublishDir
	], function(){
		return cb(null, createResult(options));
	});
}

function createResult(options){
	return  [
		{
			file: path.resolve(options.packageFilepath),
			metadata:
			{
				name: options.project,
				version: options.version,
				extension: path.extname(options.packageFilepath),
				packageType: 'browser',
				deployment: 'browser'
			}
		}
	];
}
function runNpmBuild(options, cb){

	var npm_build = spawn('npm.cmd', ['run', 'build']);
	var hasError;

	npm_build.stdout.on('data', (data) => {
	  log.debug(data.toString());
	});

	npm_build.stderr.on('data', (data) => {
		hasError = true;
	  log.error(data.toString());
	});

	npm_build.on('close', (code) => {

		if(hasError) return callback('An error occured during the npm run build command.');

		log.info('browser package completed');

		return cb(null, options);
	});
}

function updateJsFilesToPublishDir(options,cb)
{
	var packageJsFileName = path.join('./bin', [options.project.replace('.','-').toLowerCase() , '.js'].join(''));
	var publishJsFileName = options.packageFilepath;

	if(fs.existsSync(packageJsFileName))
	{
		var mapExists = fs.existsSync(packageJsFileName + '.map');

		var result = UglifyJS.minify(packageJsFileName, {
    		inSourceMap: mapExists ? packageJsFileName + '.map' : null ,
    		outSourceMap: publishJsFileName + '.map'
		});

		fs.writeFileSync(publishJsFileName, result.code);
		fs.writeFileSync(publishJsFileName + ".map", result.map);
	}

	cb(null, options);
}
